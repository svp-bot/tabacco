package server

import (
	"bytes"
	"database/sql"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"time"

	humanize "github.com/dustin/go-humanize"
)

var (
	datasetsHTML = `<!DOCTYPE html>
<html>
<head>
 <title>Tabacco</title>
</head>
<body>

  <h1>Tabacco</h1>

  <p>Started at {{fmtDate .StartTime}}.</p>

  <h3>{{.Title}}</h3>

  <table>
   <thead>
    <tr>
     <th>ID</th>
     <th>Time</th>
     <th>Host</th>
     <th>Dataset</th>
     <th>Source</th>
     <th>Atoms</th>
     <th>Files</th>
     <th>Size</th>
     <th>Time</th>
    </tr>
   </thead>
   <tbody>
    {{range $outidx, $b := .Backups}}
    {{range $idx, $d := $b.Datasets}}
    <tr>
     {{if eq $idx 0}}
     <td><b><a href="/backup/by_id?id={{$b.BackupID}}">
              {{$b.BackupID}}</a></b></td>
     <td>{{fmtDate $b.BackupTimestamp}}</td>
     <td><a href="/dataset/by_host?host={{$b.BackupHost}}">
           {{$b.BackupHost}}</a></td>
     {{else}}
     <td colspan="3"></td>
     {{end}}
     <td><a href="/dataset/by_id?id={{$d.DatasetID}}">
           {{$d.DatasetID}}</a></td>
     <td><a href="/dataset/by_source?source={{$d.DatasetSource}}">
           {{$d.DatasetSource}}</a></td>
     <td>{{$d.NumAtoms}}</td>
     <td>{{$d.DatasetTotalFiles}}</td>
     <td>{{humanBytes $d.DatasetTotalBytes}}
         ({{humanBytes $d.DatasetBytesAdded}} new)</td>
     <td>{{$d.DatasetDuration}}s</td>
    </tr>
    {{end}}
    {{end}}
   </tbody>
  </table>

</body>
</html>`
	datasetsTemplate *template.Template

	atomsHTML = `<!DOCTYPE html>
<html>
<head>
 <title>Tabacco</title>
</head>
<body>

  <h1>Tabacco</h1>

  <p>Started at {{fmtDate .StartTime}}.</p>

  <h3>Source: {{.Dataset.DatasetSource}}</h3>

  <table>
   <tbody>
    <tr>
     <td>Backup ID</td>
     <td><b><a href="/backup/by_id?id={{.Backup.BackupID}}">
             {{.Backup.BackupID}}</a></b></td>
    </tr>
    <tr>
     <td>Timestamp</td>
     <td>{{fmtDate .Backup.BackupTimestamp}}</td>
    </tr>
    <tr>
     <td>Host</td>
     <td>{{.Backup.BackupHost}}</td>
    </tr>
    <tr>
     <td>Files in dataset</td>
     <td>{{.Dataset.DatasetTotalFiles}}</td>
    </tr>
    <tr>
     <td>Bytes in dataset</td>
     <td>{{humanBytes .Dataset.DatasetTotalBytes}}
         ({{humanBytes .Dataset.DatasetBytesAdded}} new)</td>
    </tr>
    <tr>
     <td>Time elapsed</td>
     <td>{{.Dataset.DatasetDuration}} seconds</td>
    </tr>
   </tbody>
  </table>

  <h3>Atoms</h3>

  <table>
   <thead>
    <tr>
     <th>Name</th>
     <th>Path</th>
    </tr>
   </thead>
   <tbody>
    {{range .Atoms}}
    <tr>
     <td>{{.AtomName}}</td>
     <td>{{if .AtomPath}}{{.AtomPath}}{{else}}<i>stdin</i>{{end}}</td>
    </tr>
    {{end}}
   </tbody>
  </table>

</body>
</html>`
	atomsTemplate *template.Template

	startTime time.Time
)

func init() {
	funcs := map[string]interface{}{
		"fmtDate": fmtDate,
		"humanBytes": func(i int64) string {
			return humanize.Bytes(uint64(i))
		},
	}
	datasetsTemplate = template.Must(
		template.New("latest").Funcs(funcs).Parse(datasetsHTML))
	atomsTemplate = template.Must(
		template.New("source").Funcs(funcs).Parse(atomsHTML))
	startTime = time.Now()
}

type datasetDebug struct {
	DatasetID         string
	DatasetSnapshotID string
	DatasetSource     string
	DatasetTotalFiles int64
	DatasetTotalBytes int64
	DatasetBytesAdded int64
	DatasetDuration   int

	NumAtoms int
}

type backupDebug struct {
	BackupID        string
	BackupTimestamp time.Time
	BackupHost      string

	Datasets []*datasetDebug
}

func readDatasets(rows *sql.Rows) ([]*backupDebug, error) {
	tmp := make(map[string]*backupDebug)
	var backups []*backupDebug

	for rows.Next() {
		var bd backupDebug
		var dd datasetDebug
		if err := rows.Scan(
			&bd.BackupID, &bd.BackupTimestamp, &bd.BackupHost,
			&dd.DatasetID, &dd.DatasetSnapshotID, &dd.DatasetSource,
			&dd.DatasetTotalFiles, &dd.DatasetTotalBytes,
			&dd.DatasetBytesAdded, &dd.DatasetDuration, &dd.NumAtoms,
		); err != nil {
			return nil, err
		}

		b, ok := tmp[bd.BackupID]
		if !ok {
			b = &bd
			tmp[bd.BackupID] = b
			backups = append(backups, b)
		}
		b.Datasets = append(b.Datasets, &dd)
	}

	return backups, rows.Err()
}

func (s *httpServer) queryAndShowDatasets(w http.ResponseWriter, r *http.Request, title, queryName string, queryArgs ...interface{}) {
	var backups []*backupDebug

	err := retryBusy(r.Context(), func() error {
		return withTX(r.Context(), s.db, func(tx *sql.Tx) error {
			stmt := s.stmts.get(tx, queryName)
			defer stmt.Close()

			rows, err := stmt.Query(queryArgs...)
			if err != nil {
				return err
			}
			defer rows.Close()
			backups, err = readDatasets(rows)
			return err
		})
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var buf bytes.Buffer
	if err := datasetsTemplate.Execute(&buf, map[string]interface{}{
		"Title":     title,
		"Backups":   backups,
		"StartTime": startTime,
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf) // nolint
}

func (s *httpServer) handleDebug(w http.ResponseWriter, r *http.Request) {
	s.queryAndShowDatasets(w, r, "Latest backups", "get_latest_datasets", 30)
}

func (s *httpServer) handleDebugDatasetsBySource(w http.ResponseWriter, r *http.Request) {
	source := r.FormValue("source")
	if source == "" {
		http.Error(w, "No source specified", http.StatusBadRequest)
		return
	}

	s.queryAndShowDatasets(w, r, fmt.Sprintf("Latest backups for %s", source), "get_latest_datasets_by_source", source, 30)
}

func (s *httpServer) handleDebugDatasetsByHost(w http.ResponseWriter, r *http.Request) {
	host := r.FormValue("host")
	if host == "" {
		http.Error(w, "No host specified", http.StatusBadRequest)
		return
	}

	s.queryAndShowDatasets(w, r, fmt.Sprintf("Latest backups for host %s", host), "get_latest_datasets_by_host", host, 30)
}

func (s *httpServer) handleDebugBackupByID(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	if id == "" {
		http.Error(w, "No id specified", http.StatusBadRequest)
		return
	}

	s.queryAndShowDatasets(w, r, fmt.Sprintf("Backup %s", id), "get_backup_by_id", id)
}

func (s *httpServer) handleDebugDatasetByID(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("id")
	if id == "" {
		http.Error(w, "No id specified", http.StatusBadRequest)
		return
	}

	var atoms []*dbAtom
	err := retryBusy(r.Context(), func() error {
		return withTX(r.Context(), s.db, func(tx *sql.Tx) error {
			stmt := s.stmts.get(tx, "get_dataset_by_id")
			defer stmt.Close()

			rows, err := stmt.Query(id)
			if err != nil {
				return err
			}
			defer rows.Close()

			for rows.Next() {
				var atom dbAtom
				if err := rows.Scan(
					&atom.BackupID, &atom.BackupTimestamp, &atom.BackupHost,
					&atom.DatasetID, &atom.DatasetSnapshotID, &atom.DatasetSource,
					&atom.DatasetTotalFiles, &atom.DatasetTotalBytes,
					&atom.DatasetBytesAdded, &atom.DatasetDuration,
					&atom.AtomName, &atom.AtomPath, &atom.AtomFullPath,
				); err != nil {
					return err
				}
				atoms = append(atoms, &atom)
			}

			return rows.Err()
		})
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(atoms) == 0 {
		http.NotFound(w, r)
		return
	}

	var buf bytes.Buffer
	if err := atomsTemplate.Execute(&buf, map[string]interface{}{
		"Atoms":     atoms,
		"Backup":    atoms[0],
		"Dataset":   atoms[0],
		"StartTime": startTime,
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf) // nolint
}

func fmtDate(t time.Time) string {
	return t.Format(time.RFC3339)
}
