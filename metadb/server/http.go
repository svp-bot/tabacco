package server

import (
	"log"
	"net/http"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/tabacco"
)

type httpServer struct {
	*Service
}

type addDatasetRequest struct {
	Backup  tabacco.Backup  `json:"backup"`
	Dataset tabacco.Dataset `json:"dataset"`
}

func (s *httpServer) handleAddDataset(w http.ResponseWriter, r *http.Request) {
	var req addDatasetRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := s.AddDataset(r.Context(), req.Backup, req.Dataset); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("AddDataset(%+v) error: %v", req, err)
		return
	}

	serverutil.EncodeJSONResponse(w, struct{}{})
}

func (s *httpServer) handleFindAtoms(w http.ResponseWriter, r *http.Request) {
	var req tabacco.FindRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	resp, err := s.FindAtoms(r.Context(), &req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("FindAtoms(%+v) error: %v", req, err)
		return
	}

	serverutil.EncodeJSONResponse(w, resp)
}

func (s *httpServer) Handler() http.Handler {
	m := http.NewServeMux()
	m.HandleFunc("/api/add_dataset", s.handleAddDataset)
	m.HandleFunc("/api/find_atoms", s.handleFindAtoms)
	m.HandleFunc("/dataset/by_source", s.handleDebugDatasetsBySource)
	m.HandleFunc("/dataset/by_host", s.handleDebugDatasetsByHost)
	m.HandleFunc("/dataset/by_id", s.handleDebugDatasetByID)
	m.HandleFunc("/backup/by_id", s.handleDebugBackupByID)
	m.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}
		s.handleDebug(w, r)
	})
	return m
}

// Serve the specified service over HTTP with the given config.
func Serve(svc *Service, config *serverutil.ServerConfig, addr string) error {
	httpSrv := &httpServer{svc}
	return serverutil.Serve(httpSrv.Handler(), config, addr)
}
