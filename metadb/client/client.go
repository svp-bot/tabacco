package client

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/tools/tabacco"
)

type metadbClient struct {
	backend clientutil.Backend
}

// New creates a new client for a remote MetadataStore.
func New(config *clientutil.BackendConfig) (tabacco.MetadataStore, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &metadbClient{be}, nil
}

type addDatasetRequest struct {
	Backup  *tabacco.Backup  `json:"backup"`
	Dataset *tabacco.Dataset `json:"dataset"`
}

func (c *metadbClient) AddDataset(ctx context.Context, backup *tabacco.Backup, ds *tabacco.Dataset) error {
	// Ensure that the backup has no Datasets
	if len(backup.Datasets) > 0 {
		panic("AddDataset client called with non-empty backup.Datasets")
	}

	req := addDatasetRequest{
		Backup:  backup,
		Dataset: ds,
	}
	return c.backend.Call(ctx, "", "/api/add_dataset", &req, nil)
}

func (c *metadbClient) FindAtoms(ctx context.Context, req *tabacco.FindRequest) ([]*tabacco.Backup, error) {
	var resp []*tabacco.Backup
	err := c.backend.Call(ctx, "", "/api/find_atoms", req, &resp)
	return resp, err
}
