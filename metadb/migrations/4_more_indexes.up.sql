CREATE INDEX idx_log_backup_timestamp ON log (backup_timestamp);
CREATE INDEX idx_log_dataset_source ON log (dataset_source);
