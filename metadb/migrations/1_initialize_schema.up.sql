
--- Initialize the database schema.

--- Denormalized log table.
CREATE TABLE log (
        backup_id VARCHAR(128),
        backup_timestamp DATETIME,
        backup_host VARCHAR(128),
        dataset_id VARCHAR(128),
        dataset_source VARCHAR(128),
        atom_name VARCHAR(255),
        atom_full_path VARCHAR(255),
        atom_path VARCHAR(255)
);

CREATE UNIQUE INDEX idx_log_primary ON log (backup_id, dataset_id, atom_name);
CREATE INDEX idx_log_backup_id ON log (backup_id);
CREATE INDEX idx_log_backup_id_and_dataset_id ON log (backup_id, dataset_id);
