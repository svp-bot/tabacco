
ALTER TABLE log DROP COLUMN dataset_total_files;
ALTER TABLE log DROP COLUMN dataset_total_bytes;
ALTER TABLE log DROP COLUMN dataset_bytes_added;
ALTER TABLE log DROP COLUMN dataset_duration;
