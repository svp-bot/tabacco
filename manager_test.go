package tabacco

import (
	"context"
	"log"
	"path/filepath"
	"sync"
	"testing"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

type dummyMetadataEntry struct {
	backupID string
	backupTS time.Time
	dsID     string
	host     string
	source   string
	path     string
	atom     Atom
}

func (e dummyMetadataEntry) match(req *FindRequest) bool {
	if req.Pattern != "" {
		if !req.matchPattern(e.path) {
			return false
		}
	}
	if req.Host != "" && req.Host != e.host {
		return false
	}
	return true
}

func (e dummyMetadataEntry) toDataset() *Dataset {
	return &Dataset{
		ID:     e.dsID,
		Source: e.source,
	}
}

func (e dummyMetadataEntry) toBackup() *Backup {
	return &Backup{
		ID:        e.backupID,
		Timestamp: e.backupTS,
		Host:      e.host,
	}
}

type dummyMetadataStore struct {
	mx  sync.Mutex
	log []dummyMetadataEntry
}

// Argh! This is copy&pasted from server/service.go, but with minor
// modifications due to the different types... terrible.
func keepNumVersions(dbAtoms []dummyMetadataEntry, numVersions int) []dummyMetadataEntry {
	// numVersions == 0 is remapped to 1.
	if numVersions < 1 {
		numVersions = 1
	}

	count := 0
	tmp := make(map[string][]dummyMetadataEntry)
	for _, a := range dbAtoms {
		l := tmp[a.path]
		if len(l) < numVersions {
			l = append(l, a)
			count++
		}
		tmp[a.path] = l
	}
	out := make([]dummyMetadataEntry, 0, count)
	for _, l := range tmp {
		out = append(out, l...)
	}
	return out
}

func groupByBackup(dbAtoms []dummyMetadataEntry) []*Backup {
	// As we scan through dbAtoms, aggregate into Backups and Datasets.
	backups := make(map[string]*Backup)
	dsm := make(map[string]map[string]*Dataset)

	for _, atom := range dbAtoms {
		// Create the Backup object if it does not exist.
		b, ok := backups[atom.backupID]
		if !ok {
			b = atom.toBackup()
			backups[atom.backupID] = b
		}

		// Create the Dataset object for this Backup in the
		// two-level map (creating the intermediate map if
		// necessary).
		tmp, ok := dsm[atom.backupID]
		if !ok {
			tmp = make(map[string]*Dataset)
			dsm[atom.backupID] = tmp
		}
		// Match datasets by their unique ID.
		ds, ok := tmp[atom.dsID]
		if !ok {
			ds = atom.toDataset()
			tmp[atom.dsID] = ds
			b.Datasets = append(b.Datasets, ds)
		}

		// Finally, add the atom to the dataset.
		ds.Atoms = append(ds.Atoms, atom.atom)
	}

	out := make([]*Backup, 0, len(backups))
	for _, b := range backups {
		out = append(out, b)
	}
	return out
}

func (d *dummyMetadataStore) FindAtoms(_ context.Context, req *FindRequest) ([]*Backup, error) {
	d.mx.Lock()
	defer d.mx.Unlock()

	var tmp []dummyMetadataEntry
	for _, l := range d.log {
		if !l.match(req) {
			continue
		}
		tmp = append(tmp, l)
	}

	return groupByBackup(keepNumVersions(tmp, req.NumVersions)), nil
}

func (d *dummyMetadataStore) AddDataset(_ context.Context, backup *Backup, ds *Dataset) error {
	d.mx.Lock()
	defer d.mx.Unlock()

	log.Printf("AddDataset: %+v", *ds)
	for _, atom := range ds.Atoms {
		path := filepath.Join(ds.Source, atom.Name)
		d.log = append(d.log, dummyMetadataEntry{
			backupID: backup.ID,
			backupTS: backup.Timestamp,
			host:     backup.Host,
			path:     path,
			dsID:     ds.ID,
			source:   ds.Source,
			atom:     atom,
		})
	}
	return nil
}

func TestManager_Backup(t *testing.T) {
	store := &dummyMetadataStore{}
	repoSpec := RepositorySpec{
		Name: "main",
		Type: "restic",
		Params: map[string]interface{}{
			"uri":      "/tmp/restic/repo",
			"password": "testpass",
		},
	}
	handlerSpecs := []*HandlerSpec{
		&HandlerSpec{
			Name: "file1",
			Type: "file",
			Params: map[string]interface{}{
				"path": "/source/of/file1",
			},
			//PreBackupCommand: "echo hello",
		},
		&HandlerSpec{
			Name: "dbpipe",
			Type: "pipe",
			Params: map[string]interface{}{
				"backup_command":  "echo ${backup.id} ${ds.name} ${atom.names}",
				"restore_command": "cat",
			},
		},
	}
	sourceSpecs := []*SourceSpec{
		&SourceSpec{
			Name:     "source1",
			Handler:  "file1",
			Schedule: "@random_every 1h",
			Datasets: []*DatasetSpec{
				&DatasetSpec{
					Atoms: []Atom{{Name: "user1"}},
				},
				&DatasetSpec{
					Atoms: []Atom{{Name: "user2"}},
				},
			},
		},
		&SourceSpec{
			Name:            "source2",
			Handler:         "dbpipe",
			Schedule:        "@random_every 1h",
			DatasetsCommand: "echo '[{name: users, atoms: [{name: user1}, {name: user2}]}]'",
		},
	}
	queueSpec := &jobs.QueueSpec{
		Concurrency: 2,
	}

	// Run the backup.
	configMgr, err := NewConfigManager(&Config{
		Queue:        queueSpec,
		Repository:   repoSpec,
		HandlerSpecs: handlerSpecs,
		SourceSpecs:  sourceSpecs,
		DryRun:       true,
	})
	if err != nil {
		t.Fatal(err)
	}
	defer configMgr.Close()

	m, err := NewManager(context.TODO(), configMgr, store)
	if err != nil {
		t.Fatal(err)
	}
	defer m.Close()

	for _, src := range configMgr.current().SourceSpecs() {
		backup, err := m.Backup(context.TODO(), src)
		if err != nil {
			t.Fatal(err)
		}
		if backup.ID == "" || backup.Host == "" {
			t.Fatalf("empty fields in backup: %+v", backup)
		}
	}

	// Try to find atoms in the metadata store.
	// Let's try with a pattern first.
	resp, err := store.FindAtoms(context.TODO(), &FindRequest{Pattern: "source1/*", NumVersions: 1})
	if err != nil {
		t.Fatal("FindAtoms", err)
	}
	if len(resp) != 1 {
		t.Fatalf("bad FindAtoms(source1/*) response: %+v", resp)
	}
	if l := len(resp[0].Datasets); l != 2 {
		t.Fatalf("bad number of datasets returned by FindAtoms(source1/*): got %d, expected 2", l)
	}

	// A pattern matching a single atom.
	resp, err = store.FindAtoms(context.TODO(), &FindRequest{Pattern: "source1/user2"})
	if err != nil {
		t.Fatal("FindAtoms", err)
	}
	if len(resp) != 1 {
		t.Fatalf("bad FindAtoms(source1/user2) response: %+v", resp)
	}
}
