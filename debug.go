package tabacco

import (
	"html/template"
	"log"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	headerTpl = `<!doctype html>
<html lang="en">
  <head>
    <title>Tabacco - Debug</title>
    <style type="text/css">
body { background: white; font-family: "Helvetica", sans-serif; }
.table th { text-align: left; font-weight: bold; }
.table td { text-align: left; padding-right: 10px; }
.table thead tr { border-bottom: 2px solid #333; }
.error { color: #900; }
.ok { color: #090; }
.id { color: #999; }
    </style>
  </head>
  <body>
`

	indexDebugTpl = `{{template "header"}}

<h1>tabacco</h1>

<p><a href="/debug/restic">active backups</a></p>
<p><a href="/debug/jobs">job status</a></p>
<p><a href="/debug/sched">schedule</a></p>
<p><a href="/metrics">metrics</a></p>

{{template "footer"}}
`

	stateManagerDebugTpl = `{{template "header"}}
{{define "job_status"}}
<table class="table">
  <thead>
    <tr>
      <th>ID</th>
      <th>Name</th>
      <th>Status</th>
      <th>Started At</th>
      <th>Completed At</th>
      <th>Error</th>
    </tr>
  </thead>
  <tbody>
    {{range .}}
    <tr>
      <td class="id">{{.ID}}</td>
      <td>{{.Name}}</td>
      <td>{{.Status}}</td>
      <td>{{if not .StartedAt.IsZero}}{{timefmt .StartedAt}}{{end}}</td>
      <td>{{if not .CompletedAt.IsZero}}{{timefmt .CompletedAt}}{{end}}</td>
      <td>{{if .Err}}<span class="error">{{.Err}}</span>{{else if not .CompletedAt.IsZero}}<span class="ok">ok</span>{{end}}</td>
    </tr>
    {{end}}
  </tbody>
</table>
{{end}}

<h1>Jobs</h1>

<h3>Running ({{.NumRunning}})</h3>
{{template "job_status" .Running}}

<h3>Pending ({{.NumPending}})</h3>
{{template "job_status" .Pending}}

<h3>Done ({{.NumDone}})</h3>
{{template "job_status" .Done}}

{{template "footer"}}
`

	schedulerDebugTpl = `{{template "header"}}
{{define "schedule_status"}}
<table class="table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Schedule</th>
      <th>Last Run</th>
      <th>Next Run</th>
    </tr>
  </thead>
  <tbody>
    {{range .}}
    <tr>
      <td>{{.Name}}</td>
      <td>{{.Schedule}}</td>
      <td>{{if not .Prev.IsZero}}{{timefmt .Prev}}{{end}}</td>
      <td>{{if not .Next.IsZero}}{{timefmt .Next}}{{end}}</td>
      <td>{{if .LastError}}<span class="error">{{.LastError}}</span>{{else if not .Prev.IsZero}}<span class="ok">ok</span>{{end}}</td>
    </tr>
    {{end}}
  </tbody>
</table>
{{end}}

<h1>Schedule</h1>
{{template "schedule_status" .Schedule}}

{{template "footer"}}
`

	activeDebugTpl = `{{template "header"}}

<h1>Active backups</h1>

<table>
 <thead>
  <tr>
   <th>ID</th>
   <th>Dataset</th>
   <th>% Done</th>
   <th>Elapsed</th>
   <th>Files</th>
   <th>Bytes</th>
  </tr>
 </thead>
 <tbody>
 {{range $id, $a := .Active}}
  <tr>
   <td>{{$a.Backup.ID}}</td>
   <td>{{$a.Dataset.Source}}</td>
   <td>{{$a.PercentDone}}</td>
   <td>{{$a.SecondsElapsed}}s</td>
   <td>{{$a.FilesDone}}/{{$a.TotalFiles}}</td>
   <td>{{$a.BytesDone}}/{{$a.TotalBytes}}</td>
   {{if $a.HasCurrentFiles}}
  </tr>
  <tr>
   <td colspan="2"></td>
   <td colspan="4">
    {{range $a.CurrentFiles}}
    {{.}}<br>
    {{end}}
   </td>
   {{end}}
  </tr>
 {{end}}
 </tbody>
</table>

{{template "footer"}}
`

	footerTpl = `
  </body>
</html>`

	debugTpl *template.Template
)

func timefmt(t time.Time) string {
	return t.Format(time.Stamp)
}

func init() {
	debugTpl = template.New("").Funcs(template.FuncMap{
		"timefmt": timefmt,
	})
	template.Must(debugTpl.New("header").Parse(headerTpl))
	template.Must(debugTpl.New("footer").Parse(footerTpl))
	template.Must(debugTpl.New("index").Parse(indexDebugTpl))
	template.Must(debugTpl.New("state_manager_debug_page").Parse(stateManagerDebugTpl))
	template.Must(debugTpl.New("scheduler_debug_page").Parse(schedulerDebugTpl))
	template.Must(debugTpl.New("active_debug_page").Parse(activeDebugTpl))
}

// Job status debug handler.
func (a *Agent) handleStateManagerDebug(w http.ResponseWriter, r *http.Request) {
	pending, running, done := a.mgr.GetStatus()

	w.Header().Set("Content-Type", "text/html")
	if err := debugTpl.Lookup("state_manager_debug_page").Execute(w, map[string]interface{}{
		"Pending":    pending,
		"NumPending": len(pending),
		"Running":    running,
		"NumRunning": len(running),
		"Done":       done,
		"NumDone":    len(done),
	}); err != nil {
		log.Printf("state_manager_debug_page: %v", err)
	}
}

// Scheduler debug handler.
func (a *Agent) handleSchedulerDebug(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	if err := debugTpl.Lookup("scheduler_debug_page").Execute(w, map[string]interface{}{
		"Schedule": a.sched.GetStatus(),
	}); err != nil {
		log.Printf("scheduler_debug_page: %v", err)
	}
}

// Active restic processes debug handler.
func (a *Agent) handleActiveDebug(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	active.mx.Lock()
	if err := debugTpl.Lookup("active_debug_page").Execute(w, map[string]interface{}{
		"Active": active.active,
	}); err != nil {
		log.Printf("active_debug_page: %v", err)
	}
	active.mx.Unlock()
}

// Agent debug handler page, with links to the other two.
func (a *Agent) handleDebugPage(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", "text/html")
	debugTpl.Lookup("index").Execute(w, nil) // nolint
}

// StartHTTPServer starts a HTTP server that exports Prometheus
// metrics and debug information.
func (a *Agent) StartHTTPServer(addr string) {
	h := http.NewServeMux()
	h.HandleFunc("/debug/jobs", a.handleStateManagerDebug)
	h.HandleFunc("/debug/sched", a.handleSchedulerDebug)
	h.HandleFunc("/debug/restic", a.handleActiveDebug)

	// Add all the pprof handlers, they're useful for debugging.
	h.HandleFunc("/debug/pprof/", pprof.Index)
	h.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	h.HandleFunc("/debug/pprof/profile", pprof.Profile)
	h.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	h.HandleFunc("/debug/pprof/trace", pprof.Trace)

	h.Handle("/metrics", promhttp.Handler())
	h.HandleFunc("/", a.handleDebugPage)
	go http.ListenAndServe(addr, h) // nolint
}
