package tabacco

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

// The pipeHandler must work on a 1:1 dataset/atom mapping, because it
// generates a single file on the repository, and thus it can't
// distinguish multiple atoms inside it.
type pipeHandler struct {
	backupCmd     string
	restoreCmd    string
	compress      bool
	compressCmd   string
	decompressCmd string
}

const (
	defaultCompress      = false
	defaultCompressCmd   = "lz4c -3z - -"
	defaultDecompressCmd = "lz4c -d - -"
)

func newPipeHandler(name string, params Params) (Handler, error) {
	backupCmd := params.Get("backup_command")
	if backupCmd == "" {
		return nil, errors.New("backup_command not set")
	}

	restoreCmd := params.Get("restore_command")
	if restoreCmd == "" {
		return nil, errors.New("restore_command not set")
	}

	// Create the pipeHandler with defaults, which can be
	// overriden from Params.
	h := &pipeHandler{
		backupCmd:     backupCmd,
		restoreCmd:    restoreCmd,
		compress:      defaultCompress,
		compressCmd:   defaultCompressCmd,
		decompressCmd: defaultDecompressCmd,
	}
	if b, ok := params.GetBool("compress"); ok {
		h.compress = b
	}
	if s := params.Get("compress_command"); s != "" {
		h.compressCmd = s
	}
	if s := params.Get("decompress_command"); s != "" {
		h.decompressCmd = s
	}

	return h, nil
}

func (h *pipeHandler) BackupJob(rctx RuntimeContext, backup *Backup, ds *Dataset) jobs.Job {
	repo := rctx.Repo()
	cmd := fmt.Sprintf(
		"(%s)%s | %s",
		expandVars(h.backupCmd, backup, ds),
		h.compressSuffix(),
		repo.BackupStreamCmd(backup, ds),
	)
	return jobs.JobFunc(func(ctx context.Context) error {
		return repo.RunBackup(ctx, rctx.Shell(), backup, ds, cmd)
	})
}

func (h *pipeHandler) RestoreJob(rctx RuntimeContext, backup *Backup, ds *Dataset, target string) jobs.Job {
	return jobs.JobFunc(func(ctx context.Context) error {
		restoreCmd, err := rctx.Repo().RestoreStreamCmd(ctx, rctx, backup, ds, getWorkDir(ctx))
		if err != nil {
			return err
		}
		cmd := fmt.Sprintf(
			"%s | %s(%s)",
			restoreCmd,
			h.decompressPrefix(),
			expandVars(h.restoreCmd, backup, ds),
		)
		return rctx.Shell().Run(ctx, cmd)
	})
}

func (h *pipeHandler) compressSuffix() string {
	if !h.compress {
		return ""
	}
	return fmt.Sprintf(" | %s", h.compressCmd)
}

func (h *pipeHandler) decompressPrefix() string {
	if !h.compress {
		return ""
	}
	return fmt.Sprintf("%s | ", h.decompressCmd)
}

func expandVars(s string, backup *Backup, ds *Dataset) string {
	return os.Expand(s, func(key string) string {
		switch key {
		case "$":
			return key
		case "backup.id":
			return backup.ID
		case "atom.names":
			names := make([]string, 0, len(ds.Atoms))
			for _, a := range ds.Atoms {
				names = append(names, a.Name)
			}
			return strings.Join(names, " ")
		case "atom.paths":
			paths := make([]string, 0, len(ds.Atoms))
			for _, a := range ds.Atoms {
				paths = append(paths, a.Path)
			}
			return strings.Join(paths, " ")
		default:
			return os.Getenv(key)
		}
	})
}
