module git.autistici.org/ai3/tools/tabacco

go 1.15

require (
	git.autistici.org/ai3/go-common v0.0.0-20210118064555-73f00db54723
	github.com/dustin/go-humanize v1.0.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/google/subcommands v1.2.0
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-version v1.3.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/prometheus/client_golang v1.11.0
	github.com/robfig/cron/v3 v3.0.1
	gopkg.in/yaml.v2 v2.4.0
)
