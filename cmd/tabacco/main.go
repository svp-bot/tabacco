package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

// Wrapper that installs a SIGTERM handler that will cancel the
// Context passed to the Execute() method.
type withSignalHandlers struct {
	subcommands.Command
}

func (c *withSignalHandlers) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	// Set up a global root context that handles asynchronous
	// termination via signal.
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	termCh := make(chan os.Signal, 1)
	go func() {
		sig := <-termCh
		log.Printf("received signal %s, exiting", sig.String())
		cancel()
	}()
	signal.Notify(termCh, syscall.SIGINT, syscall.SIGTERM)

	return c.Command.Execute(ctx, f, args...)
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "help")
	subcommands.Register(subcommands.CommandsCommand(), "help")
	subcommands.Register(subcommands.FlagsCommand(), "help")
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	os.Exit(int(subcommands.Execute(context.Background())))
}
