package tabacco

import (
	"errors"
	"fmt"
)

// HandlerSpec defines the configuration for a handler.
type HandlerSpec struct {
	// Handler name (unique global identifier).
	Name string `yaml:"name"`

	// Handler type, one of the known types.
	Type string `yaml:"type"`

	Params Params `yaml:"params"`
}

// Parse a HandlerSpec and return a Handler instance.
func (spec *HandlerSpec) Parse(src *SourceSpec) (Handler, error) {
	if spec.Name == "" {
		return nil, errors.New("name is not set")
	}

	// Merge parameters from the handler spec and the source, with
	// preference to the latter.
	params := make(map[string]interface{})
	for k, v := range spec.Params {
		params[k] = v
	}
	for k, v := range src.Params {
		params[k] = v
	}

	switch spec.Type {
	case "file":
		return newFileHandler(spec.Name, params)
	case "pipe":
		return newPipeHandler(spec.Name, params)
	default:
		return nil, fmt.Errorf("%s: unknown handler type '%s'", spec.Name, spec.Type)
	}
}
