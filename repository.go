package tabacco

import (
	"errors"
	"fmt"
)

// RepositorySpec defines the configuration of a repository.
type RepositorySpec struct {
	Name   string `yaml:"name"`
	Type   string `yaml:"type"`
	Params Params `yaml:"params"`
}

// Parse a RepositorySpec and return a Repository instance.
func (spec *RepositorySpec) Parse() (Repository, error) {
	if spec.Name == "" {
		return nil, errors.New("name is empty")
	}

	switch spec.Type {
	case "restic":
		return newResticRepository(spec.Params)

	default:
		return nil, fmt.Errorf("unknown repository type '%s'", spec.Type)
	}
}
