#!/bin/sh

RESTIC_VERSION=${1:-0.11.0}

dpkg_arch=`dpkg-architecture -qDEB_BUILD_ARCH`
case "$dpkg_arch" in
amd64)   ARCH=amd64   ;;
i686)    ARCH=386     ;;
armel)   ARCH=arm     ;;
armhf)   ARCH=arm     ;;
arm64)   ARCH=arm64   ;;
*)
    echo "Unsupported architecture: $dpkg_arch" >&2
    exit 1
    ;;
esac

DOWNLOAD_URL="https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux_${ARCH}.bz2"

set -e
set -x

test -x /usr/bin/curl || apt-get -qy install curl
curl -sL "${DOWNLOAD_URL}" | bunzip2 -c > /usr/bin/restic
chmod +x /usr/bin/restic

exit 0
