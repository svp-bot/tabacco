package tabacco

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/tools/tabacco/jobs"
	"github.com/hashicorp/go-version"
)

type resticRepository struct {
	bin          string
	uri          string
	passwordFile string
	excludes     []string
	excludeFiles []string
	autoPrune    bool
	cacheDirPath string

	initialized sync.Once
}

func (r *resticRepository) resticCmd() string {
	args := []string{r.bin, "-r", r.uri}
	if r.passwordFile != "" {
		args = append(args, "--password-file", r.passwordFile)
	}
	for _, x := range r.excludes {
		args = append(args, "--exclude", x)
	}
	for _, x := range r.excludeFiles {
		args = append(args, "--exclude-file", x)
	}
	return strings.Join(args, " ")
}

// We need to check that we're running at least restic 0.9, or the
// restore functionality won't work as expected.
var (
	resticVersionRx      = regexp.MustCompile(`^restic ([0-9.]+)`)
	resticMinGoodVersion = "0.9"
)

func checkResticVersion(bin string) error {
	output, err := exec.Command(bin, "version").Output() // #nosec
	if err != nil {
		return err
	}
	m := resticVersionRx.FindStringSubmatch(string(output))
	if len(m) < 2 {
		return errors.New("could not parse restic version")
	}
	v, err := version.NewVersion(m[1])
	if err != nil {
		return err
	}
	log.Printf("detected restic version %s", v)
	minV, _ := version.NewVersion(resticMinGoodVersion) // nolint
	if v.LessThan(minV) {
		return fmt.Errorf("restic should be at least version %s (is %s)", minV, v)
	}
	return nil
}

// newResticRepository returns a restic repository.
func newResticRepository(params Params) (Repository, error) {
	uri := params.Get("uri")
	if uri == "" {
		return nil, errors.New("missing uri")
	}
	password := params.Get("password")
	if password == "" {
		return nil, errors.New("missing password")
	}

	bin := "restic"
	if s := params.Get("restic_binary"); s != "" {
		bin = s
	}
	if err := checkResticVersion(bin); err != nil {
		return nil, err
	}

	tmpf, err := ioutil.TempFile("", "restic-pw-")
	if err != nil {
		return nil, err
	}
	if _, err := io.WriteString(tmpf, password); err != nil {
		os.Remove(tmpf.Name()) // nolint
		return nil, err
	}
	if err := tmpf.Close(); err != nil {
		os.Remove(tmpf.Name()) // nolint
		return nil, err
	}

	autoPrune := true
	if b, ok := params.GetBool("autoprune"); ok {
		autoPrune = b
	}
	return &resticRepository{
		bin:          bin,
		uri:          uri,
		passwordFile: tmpf.Name(),
		excludes:     params.GetList("exclude"),
		excludeFiles: params.GetList("exclude_files"),
		autoPrune:    autoPrune,
		cacheDirPath: params.Get("cache_dir"),
	}, nil
}

func (r *resticRepository) Close() error {
	return os.Remove(r.passwordFile)
}

func (r *resticRepository) Init(ctx context.Context, rctx RuntimeContext) error {
	r.initialized.Do(func() {
		// Restic init will fail if the repository is already
		// initialized, ignore errors (but log them).
		if err := rctx.Shell().Run(ctx, fmt.Sprintf(
			"%s init --quiet || true",
			r.resticCmd(),
		)); err != nil {
			GetLogger(ctx).Printf("restic repository init failed (likely harmless): %v", err)
		}
	})
	return nil
}

func (r *resticRepository) Prepare(ctx context.Context, rctx RuntimeContext, backup *Backup) error {
	if !r.autoPrune {
		return nil
	}
	return rctx.Shell().Run(ctx, fmt.Sprintf(
		"%s forget --host %s --keep-last 10 --prune",
		r.resticCmd(),
		backup.Host,
	))
}

func resticBackupTags(backup *Backup, ds *Dataset) string {
	return fmt.Sprintf("--tag dataset_id=%s --tag backup_id=%s --tag dataset_source=%s", ds.ID, backup.ID, ds.Source)
}

func (r *resticRepository) cacheArgs(ds *Dataset) string {
	if r.cacheDirPath == "" {
		return "--no-cache"
	}
	dir := filepath.Join(r.cacheDirPath, strings.Replace(ds.Source, "/", "_", -1))
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err := os.MkdirAll(dir, 0700); err != nil {
			log.Printf("warning: could not create cache directory (%s), running without a local cache", err)
			return "--no-cache"
		}
	}
	return fmt.Sprintf("--cache-dir %s", dir)
}

func (r *resticRepository) BackupCmd(backup *Backup, ds *Dataset, inputFile string, exclude []string) string {
	cmd := fmt.Sprintf(
		"%s backup %s --json --exclude-caches --one-file-system %s --files-from %s",
		r.resticCmd(),
		r.cacheArgs(ds),
		resticBackupTags(backup, ds),
		inputFile,
	)
	for _, ex := range exclude {
		cmd += fmt.Sprintf(" --exclude='%s'", ex)
	}
	return cmd
}

func (r *resticRepository) getSnapshotID(ctx context.Context, rctx RuntimeContext, backup *Backup, ds *Dataset) (string, error) {
	if ds.SnapshotID != "" {
		return ds.SnapshotID, nil
	}

	// Legacy compatibility: query restic using the dataset ID.
	data, err := rctx.Shell().Output(ctx, fmt.Sprintf(
		"%s snapshots %s --no-lock --json %s",
		r.resticCmd(),
		r.cacheArgs(ds),
		resticBackupTags(backup, ds),
	))
	if err != nil {
		return "", err
	}
	snaps, err := parseResticSnapshots(data)
	if err != nil {
		return "", err
	}
	if len(snaps) < 1 {
		return "", fmt.Errorf("could not find snapshot for backup id %s", backup.ID)
	}
	return snaps[0].ShortID, nil
}

func (r *resticRepository) RestoreCmd(ctx context.Context, rctx RuntimeContext, backup *Backup, ds *Dataset, paths []string, target string) (string, error) {
	snap, err := r.getSnapshotID(ctx, rctx, backup, ds)
	if err != nil {
		return "", err
	}

	cmd := []string{
		fmt.Sprintf("%s restore", r.resticCmd()),
	}

	for _, path := range paths {
		cmd = append(cmd, fmt.Sprintf("--include %s", path))
	}

	cmd = append(cmd, fmt.Sprintf("--target %s", target))
	cmd = append(cmd, snap)
	return strings.Join(cmd, " "), nil
}

// A special path for stdin datasets that is likely to be unused by the
// rest of the filesystem (the path namespace in Restic is global).
func datasetStdinPath(ds *Dataset) string {
	var dsPath string
	switch len(ds.Atoms) {
	case 0:
		dsPath = ds.Source
	case 1:
		dsPath = filepath.Join(ds.Source, ds.Atoms[0].Name)
	default:
		dsPath = filepath.Join(ds.Source, "all")
	}
	dsPath = strings.TrimPrefix(dsPath, "/")
	dsPath = strings.Replace(dsPath, "/", "_", -1)
	return fmt.Sprintf("/STDIN_%s", dsPath)
}

func (r *resticRepository) BackupStreamCmd(backup *Backup, ds *Dataset) string {
	fakePath := datasetStdinPath(ds)
	return fmt.Sprintf(
		// The --force prevents restic from trying to find a previous snapshot.
		"%s backup %s %s --json --force --stdin --stdin-filename %s",
		r.resticCmd(),
		r.cacheArgs(ds),
		resticBackupTags(backup, ds),
		fakePath,
	)
}

func (r *resticRepository) RestoreStreamCmd(ctx context.Context, rctx RuntimeContext, backup *Backup, ds *Dataset, target string) (string, error) {
	snap, err := r.getSnapshotID(ctx, rctx, backup, ds)
	if err != nil {
		return "", err
	}

	fakePath := datasetStdinPath(ds)
	targetPath := filepath.Base(fakePath)

	// Restore the file to a temporary directory, then pipe it.
	return fmt.Sprintf(
		"(%s restore --target %s %s 1>&2 && cat %s)",
		r.resticCmd(),
		target,
		snap,
		filepath.Join(target, targetPath),
	), nil
}

// Global map that keeps track of the currently running restic
// processes and their respective progress, for debugging purposes.
type activeBackupStatus struct {
	sync.Mutex

	Backup  *Backup
	Dataset *Dataset
	Status  *resticStatusMessage
}

type activeBackups struct {
	mx     sync.Mutex
	active map[string]*activeBackupStatus
}

var active *activeBackups

func init() {
	active = &activeBackups{
		active: make(map[string]*activeBackupStatus),
	}
}

func (a *activeBackups) WithResticStatus(jobID string, backup *Backup, ds *Dataset, f func(chan *resticStatusMessage) error) error {
	ch := make(chan *resticStatusMessage, 1)

	status := &activeBackupStatus{
		Backup:  backup,
		Dataset: ds,
	}
	a.mx.Lock()
	a.active[jobID] = status
	a.mx.Unlock()

	go func() {
		for resticStatus := range ch {
			a.mx.Lock()
			status.Status = resticStatus
			a.mx.Unlock()
		}
	}()

	err := f(ch)

	close(ch)

	a.mx.Lock()
	delete(a.active, jobID)
	a.mx.Unlock()

	return err
}

// JSON status message.
type resticStatusMessage struct {
	SecondsElapsed int64    `json:"seconds_elapsed"`
	PercentDone    float64  `json:"percent_done"`
	TotalFiles     int64    `json:"total_files"`
	FilesDone      int64    `json:"files_done"`
	TotalBytes     int64    `json:"total_bytes"`
	BytesDone      int64    `json:"bytes_done"`
	CurrentFiles   []string `json:"current_files"`
}

func (m *resticStatusMessage) HasCurrentFiles() bool {
	return len(m.CurrentFiles) > 0
}

// JSON summary message.
type resticSummaryMessage struct {
	FilesNew            int64   `json:"files_new"`
	FilesChanged        int64   `json:"files_changed"`
	FilesUnmodified     int64   `json:"files_unmodified"`
	DirsNew             int64   `json:"dirs_new"`
	DirsChanged         int64   `json:"dirs_changed"`
	DirsUnmodified      int64   `json:"dirs_unmodified"`
	DataBlobs           int64   `json:"data_blobs"`
	TreeBlobs           int64   `json:"tree_blobs"`
	DataAdded           int64   `json:"data_added"`
	TotalFilesProcessed int64   `json:"total_files_processed"`
	TotalBytesProcessed int64   `json:"total_bytes_processed"`
	TotalDuration       float64 `json:"total_duration"`
	SnapshotID          string  `json:"snapshot_id"`
}

// Generic JSON message from 'restic backup'.
type resticMessage struct {
	MessageType string `json:"message_type"`
	resticStatusMessage
	resticSummaryMessage
}

// Scan the output of 'restic backup' for the snapshot ID. Modifies backup/dataset objects.
func (r *resticRepository) RunBackup(ctx context.Context, shell *Shell, backup *Backup, ds *Dataset, cmd string) error {
	return active.WithResticStatus(jobs.GetID(ctx), backup, ds, func(progressCh chan *resticStatusMessage) error {
		return shell.RunWithStdoutCallback(ctx, cmd, func(line []byte) {
			var msg resticMessage
			if err := json.Unmarshal(line, &msg); err != nil {
				GetLogger(ctx).Printf("error parsing restic JSON message: %v (%s)", err, line)
				return
			}
			switch msg.MessageType {
			case "status":
				progressCh <- &msg.resticStatusMessage
			case "summary":
				GetLogger(ctx).Printf("dataset %s: total_bytes=%d, new=%d", ds.ID, msg.TotalBytesProcessed, msg.DataAdded)
				ds.SnapshotID = msg.SnapshotID
				ds.TotalFiles = msg.TotalFilesProcessed
				ds.TotalBytes = msg.TotalBytesProcessed
				ds.BytesAdded = msg.DataAdded
				ds.Duration = int(msg.TotalDuration)
			}
		})
	})
}

// Data about a snapshot, obtained from 'restic snapshots --json'.
type resticSnapshot struct {
	ID       string    `json:"id"`
	ShortID  string    `json:"short_id"`
	Time     time.Time `json:"time"`
	Parent   string    `json:"parent"`
	Tree     string    `json:"tree"`
	Hostname string    `json:"hostname"`
	Username string    `json:"username"`
	UID      int       `json:"uid"`
	GID      int       `json:"gid"`
	Paths    []string  `json:"paths"`
	Tags     []string  `json:"tags"`
}

func parseResticSnapshots(output []byte) (snapshots []resticSnapshot, err error) {
	// An empty input is not necessarily an error.
	if len(output) > 0 {
		err = json.Unmarshal(output, &snapshots)
	}
	return
}
