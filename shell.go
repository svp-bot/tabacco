package tabacco

import (
	"bufio"
	"context"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"git.autistici.org/ai3/tools/tabacco/jobs"
)

// Environment variables starting with any of the following strings
// are removed from the command execution environment.
var filteredEnvVars = []string{
	"LANG=",
	"LC_",
	"RESTIC_",
}

// Return a copy of os.Environ(), with filteredEnvVars removed.
func safeEnv() []string {
	var env []string
	for _, s := range os.Environ() {
		skip := false
		for _, pfx := range filteredEnvVars {
			if strings.HasPrefix(s, pfx) {
				skip = true
				break
			}
		}
		if !skip {
			env = append(env, s)
		}
	}
	return env
}

// Shell runs commands, with some options (a global dry-run flag
// preventing all executions, nice level, i/o class). As one may guess
// by the name, commands are run using the shell, so variable
// substitutions and other shell features are available.
type Shell struct {
	dryRun      bool
	niceLevel   int
	ioniceClass int
	env         []string
}

// NewShell creates a new Shell.
func NewShell(dryRun bool) *Shell {
	return &Shell{
		dryRun:      dryRun,
		niceLevel:   10,
		ioniceClass: 2,
		env:         safeEnv(),
	}
}

// SetNiceLevel sets the nice(1) level.
func (s *Shell) SetNiceLevel(n int) {
	s.niceLevel = n
}

// SetIOClass sets the ionice(1) i/o class.
func (s *Shell) SetIOClass(n int) {
	s.ioniceClass = n
}

// command builds an exec.Cmd and gets some parameters from the
// context - notably it sets log output and working directory to be in
// the working dir if the job has been wrapped in WithWorkDir().
func (s *Shell) command(ctx context.Context, arg string) *exec.Cmd {
	var args []string
	if s.dryRun {
		args = []string{"/bin/true", arg}
	} else {
		// The pipefail option is necessary for us to detect
		// when the first command in a pipeline fails, but we
		// need bash for that.
		args = []string{"/bin/bash", "-o", "pipefail", "-c", arg}
	}

	if s.niceLevel != 0 {
		args = append(
			[]string{"nice", "-n", strconv.Itoa(s.niceLevel)},
			args...,
		)
	}
	if s.ioniceClass != 0 {
		args = append(
			[]string{"ionice", "-c", strconv.Itoa(s.ioniceClass)},
			args...,
		)
	}

	c := exec.CommandContext(ctx, args[0], args[1:]...) // #nosec
	var env []string
	env = append(env, s.env...)
	c.Env = env
	c.Dir = getWorkDir(ctx)

	l := GetLogger(ctx)
	if jobID := jobs.GetID(ctx); jobID != "" {
		l.Printf("job=%s: sh: %s", jobID, arg)
	} else {
		l.Printf("sh: %s", arg)
	}

	return c
}

// RunWithStdoutCallback executes a command and invokes a callback on
// every line read from its standard output. Stdandard output and
// error are still logged normally as in Run().
func (s *Shell) RunWithStdoutCallback(ctx context.Context, arg string, stdoutCallback func([]byte)) error {
	c := s.command(ctx, arg)

	stdout, err := c.StdoutPipe()
	if err != nil {
		return err
	}
	stderr, err := c.StderrPipe()
	if err != nil {
		return err
	}

	if err := c.Start(); err != nil {
		return err
	}

	l := GetLogger(ctx)
	go func() {
		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			if stdoutCallback != nil {
				stdoutCallback(scanner.Bytes())
			} else {
				l.Printf("%s", scanner.Bytes())
			}
		}
	}()
	go func() {
		scanner := bufio.NewScanner(stderr)
		for scanner.Scan() {
			l.Printf("stderr: %s", scanner.Bytes())
		}
	}()
	return c.Wait()
}

// Run a command. Log its standard output and error.
func (s *Shell) Run(ctx context.Context, arg string) error {
	return s.RunWithStdoutCallback(ctx, arg, nil)
}

// Output runs a command and returns the standard output.
func (s *Shell) Output(ctx context.Context, arg string) ([]byte, error) {
	c := s.command(ctx, arg)
	c.Stderr = os.Stderr
	return c.Output()
}

// Manage a work directory (temporary scratch with lots of space).
type workdirManager struct {
	dir string
}

func newWorkdirManager(tmpdir string) (*workdirManager, error) {
	dir, err := ioutil.TempDir(tmpdir, "tabacco-")
	if err != nil {
		return nil, err
	}
	return &workdirManager{dir}, nil
}

func (w *workdirManager) Close() {
	os.RemoveAll(w.dir) // nolint
}

func (w *workdirManager) withWorkDir(j jobs.Job) jobs.Job {
	jobDir := filepath.Join(w.dir, "job-"+j.ID())
	return &workdirJob{
		Job: j,
		dir: jobDir,
	}
}

// Give a Job its own temporary directory.
type workdirJob struct {
	jobs.Job
	dir string
}

type workdirKeyType int

var workdirKey workdirKeyType = 1

func (j *workdirJob) RunContext(ctx context.Context) error {
	if err := os.Mkdir(j.dir, 0700); err != nil {
		return err
	}

	ctx = context.WithValue(ctx, workdirKey, j.dir)
	return j.Job.RunContext(ctx)
}

// getWorkDir returns the current working directory, provided the
// WithWorkDir wrapper has been called.
func getWorkDir(ctx context.Context) string {
	dir, ok := ctx.Value(workdirKey).(string)
	if ok {
		return dir
	}
	return "/tmp"
}
