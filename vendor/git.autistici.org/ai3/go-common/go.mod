module git.autistici.org/ai3/go-common

go 1.11

require (
	contrib.go.opencensus.io/exporter/zipkin v0.1.2
	github.com/amoghe/go-crypt v0.0.0-20191109212615-b2ff80594b7f
	github.com/bbrks/wrap/v2 v2.5.0
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594
	github.com/go-asn1-ber/asn1-ber v1.5.3
	github.com/go-ldap/ldap/v3 v3.2.4
	github.com/gofrs/flock v0.8.0 // indirect
	github.com/google/go-cmp v0.5.4
	github.com/gorilla/handlers v1.5.1
	github.com/lunixbochs/struc v0.0.0-20200707160740-784aaebc1d40
	github.com/miscreant/miscreant.go v0.0.0-20200214223636-26d376326b75
	github.com/openzipkin/zipkin-go v0.2.5
	github.com/prometheus/client_golang v1.9.0
	github.com/russross/blackfriday/v2 v2.1.0
	github.com/theckman/go-flock v0.8.0
	github.com/tstranex/u2f v1.0.0
	go.opencensus.io v0.22.5
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
)
